import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import javafx.embed.swing.SwingFXUtils;

import java.io.IOException;
//import java.net.URL;
//import javax.imageio.ImageIO;

import java.util.Date;
import java.util.Calendar;
import java.text.DecimalFormat;
import java.lang.Math; 

import com.google.gson.*;

public class WxModel
{
    private JsonElement jse;
	public boolean getWx(String zipCode)
	{
        try
		{
			// Construct WxStation API URL
			URL wxURL = new URL("http://api.openweathermap.org/data/2.5/weather?zip="
					+ zipCode
					+ "&units=imperial&appid=e80ee2d08966daf311b22cc9b7244f00");

			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
        catch (java.io.UnsupportedEncodingException uee)
        {
            uee.printStackTrace();
        }
        catch (java.net.MalformedURLException mue)
        {
            mue.printStackTrace();
        }
        catch (java.io.IOException ioe)
        {
            //ioe.printStackTrace();
            return false;
        }
        catch (java.lang.NullPointerException npe)
        {
            npe.printStackTrace();
              
        }
        
        // Check to see if the zip code was valid.
        return isValid();
        
    }
    
    public boolean isValid()
    {
        // If the zip is not valid we will get an error field in the JSON
        try {
            String error = jse.getAsJsonObject().get("message").getAsString();
            return false;
        }

        catch (java.lang.NullPointerException npe)
        {
            // We did not see error so this is a valid zip
            return true;
        }
    }
    
    public String getLocation()
    {
        return jse.getAsJsonObject().get("name").getAsString();
    }

    public String getTime()
    {
        String dt = jse.getAsJsonObject().get("dt").getAsString();
        Date date = new Date(Long.valueOf(dt)*1000);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String time, monthString, minutesString;
            
        int minutes = date.getMinutes();
        int hours = date.getHours();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
            
        if (minutes < 10) {
            minutesString = "0" + minutes;
        } else {
            minutesString = "" + minutes;
        }
        
        if (hours <= 12) {
            time = hours + ":" + minutesString + " AM";
        } else {
            time = hours % 12 + ":" + minutesString + " PM";
        }
        switch (month) {
            case 1 : monthString = "January"; break;
            case 2 : monthString = "February"; break;
            case 3 : monthString = "March"; break;
            case 4 : monthString = "April"; break;
            case 5 : monthString = "May"; break;
            case 6 : monthString = "June"; break;
            case 7 : monthString = "July"; break;
            case 8 : monthString = "August"; break;
            case 9 : monthString = "September"; break;
            case 10: monthString = "October"; break;
            case 11: monthString = "November"; break;
            case 12: monthString = "December"; break;
            default: monthString = "Invalid month"; break;
        }
        return monthString + " " + day + ", " + time + " PDT";
    }

    public String getWeather()
    {
       return jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("main").getAsString();
    }

    public String getTemperature()
    {
        String temp =  jse.getAsJsonObject().getAsJsonObject("main").get("temp").getAsString();
        double temperature = Double.valueOf(temp);
        DecimalFormat df = new DecimalFormat("###0.0");
        String display = df.format(temperature);
        return display;
    }

    public String getWind()
    {
        String windspeed = jse.getAsJsonObject().getAsJsonObject("wind").get("speed").getAsString();
        double speed = Double.valueOf(windspeed);
        DecimalFormat df = new DecimalFormat("###0.0");
        windspeed = df.format(speed);
        
        String direction = jse.getAsJsonObject().getAsJsonObject("wind").get("deg").getAsString();
        double degrees = Double.valueOf(direction);
        degrees = degrees % 360.0;
        
        if (degrees >= 337.5 && degrees <= 360 || degrees >= 0 && degrees < 22.5)
            direction = "N";
        else if (degrees >= 22.5 && degrees < 67.5)
            direction = "NE";
        else if (degrees >= 67.5 && degrees < 112.5)
            direction = "E";
        else if (degrees >= 112.5 && degrees < 157.5)
            direction = "SE";
        else if (degrees >= 157.5 && degrees < 202.5)
            direction = "SE";
        else if (degrees >= 202.5 && degrees < 247.5)
            direction = "SW";
        else if (degrees >= 247.5 && degrees < 292.5)
            direction = "W";
        else if (degrees >= 292.5 && degrees < 337.5)
            direction = "NW";
        else
            direction = "Invalid Direction";
        
        return windspeed + " mph " + direction;    
    }

    public String getPressure()
    {
        String press = jse.getAsJsonObject().getAsJsonObject("main").get("pressure").getAsString();
        double pressure = Double.valueOf(press) * 0.029529983071445;
        DecimalFormat df = new DecimalFormat("####0.00");
        String display =  df.format(pressure) + " in HG";
        return display;
    }

    public String getHumidity()
    {
        return jse.getAsJsonObject().getAsJsonObject("main").get("humidity").getAsString() + "%";
    }

    public Image getImage()
    {   
        String code = jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("icon").getAsString();        
        BufferedImage image = null;
        try {
            URL url = new URL("http://openweathermap.org/img/w/" + code + ".png");
            image = ImageIO.read(url);
        } catch (IOException e) {
        	e.printStackTrace();
        }
        Image icon = SwingFXUtils.toFXImage(image, null);
        return icon;
        
    }
        
}
